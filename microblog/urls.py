from django.conf.urls import patterns, include, url
from django.views.generic import ListView, DetailView, UpdateView
from microblog import views
from models import *
from forms import *

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^index/$', views.index, name='index'),
    url(r'^login/$', views.log_in, name='log_in'),
    url(r'^logout/$', views.log_out, name='log_out'),
    url(r'^register/$', views.register, name='register'),
    url(r'^add/$', views.add, name='add'),
    url(r'^add/index/$', views.index, name='index'),
    url(r'^added/$', views.added, name='added'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^users/(?P<id>\d+)/$', views.users, name='users'),
    url(r'^tags/(?P<id>\d+)/$', views.tags, name='tags'),
    url(r'^edit/(?P<id>\d+)/$', views.edit, name='edit'),
    url(r'^selec/$', views.selec, name='selec'),
    url(r'^my/$', views.useres, name='my'),
    url(r'^sign/$', views.sign, name='sign'),
    url(r'^newpass', views.newpass, name='newpass'),
    url(r'^start', views.start, name='start')
)

urlpatterns += patterns('', (
    r'^static/(?P<path>.*)$',
    'django.views.static.serve',
    {'document_root': 'static'}
))