from django.forms import ModelForm, Textarea, CharField
from django import forms
from models import *

class EntryForm(ModelForm):
    class Meta:
        model = Entry

class PartialEntryForm(ModelForm):
    class Meta:
        model = Entry
        exclude = ('author', 'pub_date')