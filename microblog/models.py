from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Entry(models.Model):
    author = models.ForeignKey(User, related_name='+')
    text = models.TextField(max_length=400)
    pub_date = models.DateTimeField()

    def __unicode__(self):
        return self.text

    def chek_user(self):
        return 1==1