# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
from django.contrib import messages
from django.utils import timezone
from models import *
from forms import *
from django.shortcuts import render_to_response
from django.core.mail import send_mail, BadHeaderError

entries = Entry.objects.all().order_by('-pk')
Tages = ''
Users = User.objects.all()

def index(request):
    user = request.user
    entries = Entry.objects.all().order_by('-pk')
    Users = User.objects.all()
    return render(request, 'microblog/index.html', {'user': user, 'entries': entries, 'users': Users, 'tages': Tages})

def start(request):
    user = request.user
    entries = Entry.objects.all().order_by('-pk')
    Users = User.objects.all()
    return render(request, 'microblog/start.html', {'user': user, 'entries': entries, 'users': Users, 'tages': Tages})

def log_in(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        if request.POST.get('go') == 'Login':
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return render(request, 'microblog/index.html', {'user':user, 'entries': entries, 'mess':'Poprawnie zalogowano!', 'users': Users, 'tages': Tages})
            else:
                return render(request, 'microblog/login.html', {'message':'Invalid username or password!', 'users': Users, 'tages': Tages})
        elif request.POST.get('go') == 'Forgot password ?':
            if username is None or username == '':
                messa = 'First insert username !'
            else:
                try:
                    emal = []
                    uses = User.objects.get(username__exact=username)
                    emal.append(uses.email)
                    newrandompassword = User.objects.make_random_password(length=10, allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789')
                    uses.set_password(newrandompassword)
                    uses.save()
                    etytul = "New Password to My Site - Blog"
                    etresc = "Hi, "+uses.username+"\n\nHere is your new password to our site:\n"+newrandompassword+"\n\nWe wait for you in 194.29.175.240/~p7/mysite/blog/login\nTeam Blog"
                    send_mail(etytul, etresc, 'microblog@mysite.pl', emal)
                    messa = 'New Password is in your mail !'#+newrandompassword#: '+str(emal) +' !'
                except:
                    messa = 'Invalid username !'
                    #messa = newrandompassword + " na " + str(emal)
            return render(request, 'microblog/login.html', {'message': messa, 'users': Users, 'tages': Tages})
    else:
        return render(request, 'microblog/login.html', {'users': Users, 'tages': Tages})

def log_out(request):
    logout(request)
    return render(request, 'microblog/index.html', {'entries': entries, 'mess':'Poprawnie wylogowano!', 'users': Users, 'tages': Tages})

def register(request):
    #entries = Entry.objects.all().order_by('-pk')
    #if request.method == "POST":
    #    username = request.POST.get('username')
    #    password = request.POST.get('password')
    #    confirmpassword = request.POST.get('confirmpassword')
    #    if password == confirmpassword:
    #        user = User.objects.create_user(username, None, password)
    #        user.is_staff = True
    #        user.full_clean()
    #        user.save()
    #        return render(request, 'microblog/index.html', {'user': user, 'entries': entries, 'mess':'Poprawnie zarejestrowano! Zaloguj sie', 'users': Users, 'tages': Tages})
    #    else:
    #        return render(request, 'microblog/register.html', {'message':'Password is not confirmed!', 'users': Users, 'tages': Tages})
    #else:
    return render(request, 'microblog/register.html', {'users': Users, 'tages': Tages})

def add(request):
    if request.user.is_authenticated():
        user = request.user
        form = PartialEntryForm()
        form.author = user
        if request.method == "POST":
            form = PartialEntryForm(request.POST)  # A form bound to the POST data
            if form.is_valid():  # All validation rules pass
                # Process the data in form.cleaned_data
                # ...
                form2 = Entry(author=request.user, pub_date=timezone.now())
                form = PartialEntryForm(request.POST, instance=form2)
                form.save()
                return HttpResponseRedirect('index') # Redirect after POST
        return render(request, 'microblog/add.html', {'form': form, 'user':user, 'users': Users, 'tages': Tages})
    else:
        return render(request, 'microblog/login.html', {'users': Users, 'tages': Tages})

def added(request):
    if request.method == "POST":
        text = request.POST.get('text')
        entry = Entry()
        entry.author = request.user
        entry.pub_date = datetime.datetime.now()
        entry.text = text
        entry.save()
        return render(request, 'microblog/index.html', {'user': request.user, 'entries': entries, 'mess': 'dodano wpis', 'users': Users, 'tages': Tages})
    else:
        return render(request, 'microblog/add.html', {'users': Users, 'tages': Tages})

def users(request, id):
    user = request.user
    if id != 0:
        userw = User.objects.filter(id=id)
        entries = Entry.objects.filter(author=userw).order_by('-pk')
    else:
        entries = Entry.objects.all().order_by('-pk')
    return render(request, 'microblog/index.html', {'user': user, 'entries': entries, 'users': Users, 'tages': Tages})

def useres(request):
    user = request.user
    entries = Entry.objects.filter(author=user).order_by('-pk')
    return render(request, 'microblog/index.html', {'user': user, 'entries': entries, 'users': Users, 'tages': Tages})

def tags(request, id):
    user = request.user
    tagr = Tags.objects.filter(id=id)
    entries = Entry.objects.filter(tags=tagr).order_by('-pk')
    return render(request, 'microblog/index.html', {'user': user, 'entries': entries, 'users': Users, 'tages': Tages})

def edit(request, id):
    if request.user.is_authenticated():
        user = request.user
        form2 = Entry.objects.get(id=id)
        author = form2.author
        pubdate = form2.pub_date
        if request.method == "POST":
            form = PartialEntryForm(request.POST)  # A form bound to the POST data
            if form.is_valid():  # All validation rules pass
                # Process the data in form.cleaned_data
                # ...
                form2.edit_user = request.user
                form2.edit_date=timezone.now()
                form = PartialEntryForm(request.POST, instance=form2)
                form.save()
                return HttpResponseRedirect('/~p7/mysite/blog/') # Redirect after POST
        else:
            form = PartialEntryForm(instance=form2)
        return render(request, 'microblog/edit.html', {'form': form, 'author':author, 'pubdate':pubdate, 'user':user, 'users': Users, 'tages': Tages})
    else:
        return render(request, 'microblog/login.html', {'users': Users, 'tages': Tages})

def sign(request):
    passwor = "Login juz zajety!"
    if request.method == "POST":
        #password = request.POST.get('password')
        #password2 = request.POST.get('confirmpassword')
        #if password == password2:
            try:
                username = request.POST.get('username')
                mail = request.POST.get('mail')
                password = User.objects.make_random_password(length=10, allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789')
                user = User.objects.create_user(username, mail, password)
                user.is_staff = True
                user.save()
                passwor = 'Your password: ' + password
                etytul = "Activate account on My Site - Blog"
                etresc = "Hi, "+username+"\n\nHere is your password to our site:\n"+password+"\n\nWe wait for you in 194.29.175.240/~p7/mysite/blog/login\nTeam Blog"
                emal = []
                emal.append(mail)
                send_mail(etytul, etresc, 'microblog@mysite.pl', emal)
                mess = 'Konto utworzone. Haslo wyslane na twojego maila !'#: '+str(emal) +' !'
            except:
                return render(request, 'microblog/register.html', {'mess':passwor, 'users': Users, 'tages': Tages})
            return render(request, 'microblog/login.html', {'message':'Konto utworzone. Haslo wyslane na twojego maila !', 'users': Users, 'tages': Tages})
        #else:
            #return render(request, 'microblog/register.html', {'mess':'Przepisz prawidlowo haslo !', 'users': Users, 'tages': Tages})
    #else:
        #return render(request, 'microblog/register.html', {'mess':'Blad przy polczeniu, sprobuj jeszcze raz.', 'users': Users, 'tages': Tages})

def newpass(request):
    if request.method == "POST":
        user = request.user
        old_password = request.POST.get('oldpassword')
        new_password = request.POST.get('newpassword')
        new_password2 = request.POST.get('confirmpassword')
        if user.check_password(old_password) and new_password == new_password2:
            u = User.objects.get(username__exact=user.username)
            u.set_password(new_password)
            u.save()
            logout(request)
            return render(request, 'microblog/login.html', {'mess':'Haslo zmieniono !', 'users': Users, 'tages': Tages})
        else:
            return render(request, 'microblog/newpassword.html', {'mess':'Bledne dane !', 'users':Users, 'tages': Tages})
    else:
        return render(request, 'microblog/newpassword.html', {'users':Users, 'tages': Tages})

def selec(request):
    liczbaa = request.POST.get('herolist')
    if liczbaa != 0:
        user = User.objects.filter(id=liczbaa)
        entries = Entry.objects.filter(author=user).order_by('-pk')
    return render(request, 'microblog/index.html', {'user': user, 'entries': entries, 'users': Users, 'tages': Tages})